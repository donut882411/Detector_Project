import os
import ImageHelper as ih
import cv2 as cv

# all the legal image extensions for OpenCV
acceptable_extensions = {'jpg', 'png', 'bmp','pbm', 'pgm', 'ppm','sr', 'ras','jpeg','webp', 'jpe', 'tiff', 'tif'}
def meassure_every_image(dir_path, deep_mode):
    # deep mode == 1 show info in terminal
    # deep mode == 2 show steps of image processing
    # search files in dir_path
    obj = os.scandir(dir_path)

    # temporary variables to check accuracy
    correct_detected = 0
    images_amount = 0

    # temporary variables to create evaluation matrices
    rows, cols = (5, 5)
    original_evaluation_matrix = [[0] * rows for i in range(cols)]
    estimation_evaluation_matrix = [[0] * rows for i in range(cols)]



    # go through every file in dir_path
    for el in obj:
        # check if is a file
        if el.is_file():
            # get extension
            extension = el.name.format().split('.')[1]
            # process only the acceptable file types
            if str(extension) in acceptable_extensions:

                # read the correct fullness
                correct_fullness = el.name.format().split('.')[0].split('_')[1]

                # read image from dir_path
                image = cv.imread(f'{dir_path}/{el.name}')

                # estimate fullness of the bottle
                measured_fullness = read_bottle_content(image)
                if deep_mode >= 1:
                    print(el.name)
                    print('height:\t\t', image.shape[0])
                    print('width:\t\t', image.shape[1])
                    print('measured:\t', measured_fullness, '\t', 'correct:\t', int(correct_fullness))


                # variables to store data in evaluation matrices
                r = int((int(measured_fullness) / 100) * 4)
                c = int((int(correct_fullness) / 100) * 4)

                # store the values
                original_evaluation_matrix[c][c] += 1
                estimation_evaluation_matrix[r][c] += 1

                if r == c:
                    correct_detected += 1
                    if deep_mode >= 1:
                        print('correct detected')
                if deep_mode >= 1:
                    print('--------------------------------')

                images_amount += 1
                if deep_mode >= 2:
                    cv.waitKey(0)
                    cv.destroyAllWindows()
    print('correct detected: ', correct_detected, ' from: ', images_amount, '(',
          (correct_detected / images_amount) % 100, '%)')

    # show the evaluation matrices
    print('--- evaluation matrices------------------')
    print('--- should be ---------------------------')
    show_evaluation_matrix(original_evaluation_matrix, 'org')
    print('--- is ----------------------------------')
    show_evaluation_matrix(estimation_evaluation_matrix, 'est')

    obj.close()

def meassure_every_image_external(dir_path, train_name, deep_mode):
    # function used to test my algorithm on external datasets
    # train has to be in the dir_path
    # deep mode == 1 show info in terminal
    # deep mode == 2 show steps of image processing

    # temporary variables to check accuracy
    correct_detected = 0
    images_amount = 0

    # temporary variables to create evaluation matrices
    rows, cols = (5, 5)
    original_evaluation_matrix = [[0] * rows for i in range(cols)]
    estimation_evaluation_matrix = [[0] * rows for i in range(cols)]

    with open(f'{dir_path}/{train_name}', 'r') as file:
        data = file.read().splitlines()

    for d in data:
        file_name = d.split(';')[0]
        extension = file_name.split('.')[1]
        if extension in acceptable_extensions:


            # read the correct fullness

            # read image from dir_path
            image = cv.imread(f'{dir_path}/{file_name}')
            correct_fullness = d.split(';')[1]
            image = ih.crop(image, (image.shape[0] // 6, 0),(image.shape[1] - image.shape[1] // 6, image.shape[0]))
            
            # estimate fullness of the bottle
            measured_fullness = read_bottle_content(image)
            # print most important infos
            if deep_mode >= 1:
                print(file_name)
                print('height:\t\t', image.shape[0])
                print('width:\t\t', image.shape[1])
                print('measured:\t', measured_fullness, '\t', 'correct:\t', int(correct_fullness))

            # variables to store data in evaluation matrices
            r = int((int(measured_fullness) / 100) * 4)
            c = int((int(correct_fullness) / 100) * 4)

            # store the values
            original_evaluation_matrix[c][c] += 1
            estimation_evaluation_matrix[r][c] += 1

            if r == c:
                correct_detected += 1
                if deep_mode >= 1:
                    print('correct detected')
            images_amount += 1
            print('--------------------------------')
            if deep_mode >= 2:
                cv.waitKey(0)
                cv.destroyAllWindows()


    print('correct detected: ', correct_detected, ' from: ', images_amount, '(',(correct_detected / images_amount) % 100, '%)')

    # show the evaluation matrices
    print('--- evaluation matrices------------------')
    print('--- should be ---------------------------')
    show_evaluation_matrix(original_evaluation_matrix, 'org')
    print('--- is ----------------------------------')
    show_evaluation_matrix(estimation_evaluation_matrix, 'est')

def read_bottle_content(image):
    image_proc = do_image_processing(image.copy())
    # original image needed for background color estimation
    class_id = do_image_prediction(image_proc.copy(), image)
    return class_id
def show_evaluation_matrix(matrix, txt):
    # first line
    print(f'{txt[0:4]}\t\t0\t\t25\t\t50\t\t75\t\t100')
    for x in range(0, len(matrix)):
        line = ''
        # prefix for every line
        line = line + f'{x * 25}'
        for y in range(0, len(matrix[x])):
            # append the values
            line = line + '\t\t' + str(matrix[x][y])
        print(line)
def do_image_processing(image):
    bottle = image.copy()
    # resize for better performance
    bottle = cv.resize(bottle, (bottle.shape[1] // 2, bottle.shape[0] // 2))

    # get the image of bottle
    bottle = ih.crop_automatically(bottle)
    # cv.imshow('cropped automatically', bottle)


    return bottle
def do_image_prediction(image_pro, org_image):
    bottle = image_pro.copy()

    # use less pixels -> better performance
    mid_rectangle = ih.detect_middle_rectangle_of_image(bottle, 128)

    # returns the measured fullness 0.00, 0.25, 0.50, 0.75, 1.00
    fluid_surface = ih.estimate_fluid_surface(mid_rectangle, 92, ih.estimate_background_color(org_image))

    # cv.imshow(f'{fluid_surface}', image_pro)
    return int(fluid_surface * 100)


if __name__ == '__main__':
    # pth = os.path.abspath('.')+'/digitale-bildverarbeitung/DetectorProject/'


    # remove the hashtag in order to test your own images
    # meassure_every_image_external('.' + '/example', 'correct_fullness.txt', 1)

    # test for my own images with train.txt
    meassure_every_image_external('.', 'train.txt', 2)

    # test for other images
    meassure_every_image_external('.' + '/imagesKreuer', 'train.txt', 2)

