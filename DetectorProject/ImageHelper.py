import random
import cv2 as cv
import numpy as np
import time as tm


def crop(image, p1, p2):
    # crop the image between 2 points
    # first point = left top corner
    # second point = right bottom corner
    return image[p1[1]: p2[1], p1[0]: p2[0]]


def show_segment_lines(image, segments_amount, divisor_value):
    # helper function that displays exact segments lines
    height = image.shape[0]
    width = image.shape[1]
    step = width / segments_amount

    tmp_image = image.copy()
    j = 0
    for i in range(0, segments_amount):
        color = (80, 80, 80)
        thickness = 1
        if j % divisor_value == 0:
            color = (150, 150, 150)
            thickness = 2

        cv.line(tmp_image, (int(i * step), 0), (int(i * step), height), color, thickness)
        j += 1
    preview(tmp_image)


def detect_x_y_points(image, segments_amount, divisor_value):
    # crops grayscale image to area where bottle probably occur

    # find vertical lines in segments
    # find horizontal lines

    # dont impact original image
    temp_image = image.copy()
    # work on binary images
    temp_image = binarize(temp_image, 18)

    # morphological operations
    # leave the vertical lines
    kernel = np.ones((5, 1), np.uint8)
    temp_image = cv.erode(temp_image, kernel, iterations=1)

    # remove background noises
    kernel = np.ones((2, 2), np.uint8)
    # temp_image = cv.erode(temp_image, kernel, iterations=1)

    # shows Approximator(widee lines) and Remover segments(narrow lines)
    show_segment_lines(temp_image, segments_amount, divisor_value)

    cv.imshow('image after Morphological operations', temp_image)

    # width = exact width of the image
    width = image.shape[1]

    # segments = amount of segments to divide the image into
    # NOTE take some nice 2 powers for segments
    segments = segments_amount

    # counters = how many white pixels occur in each segment
    counters = []

    # step = width of each segment (will be casted to int later )
    step = width / segments

    # build the segments
    # get segments corner points
    segments_points = segment_image(image, segments_amount, 0)
    # build segments images from corner point
    for i in range(0, segments_amount):
        tmp = crop(temp_image, (segments_points[i][0][0], segments_points[i][0][1]),
                   (segments_points[i][1][0], segments_points[i][1][1]))
        counters.append(count_pixels_in_intervall(tmp, 150, 255))

    # -----APPROXIMATOR-----

    # largest_counter = position of segment with most light pixels
    largest_counter = 0
    largest_counter_v = max(counters)
    # find index of segment with most light pixels
    for i in range(0, len(counters)):
        if largest_counter_v == counters[i]:
            largest_counter = i
            break

    # divisor = control in how many neighbour segments the light pixels be summed up together
    # how many neighbour segments will be 'merged' together
    divisor = divisor_value

    # minimal value to classify neighbour segments as significant
    l_r_cutoff_amount = 50

    # find the left margin of bottle
    left = 0
    # go from segment where most light pixel occur to left side
    for i in range(largest_counter, -1, -divisor):
        # start index of 'merged' segments
        start_index = largest_counter - divisor - 1

        # check the overflow
        if start_index < 0: start_index = 0

        # end index of 'merged' segments
        end_index = i + 1

        # sum up the light pixels
        s = sum(counters[start_index: end_index + 1])
        # check if the 'merged' segments are significant
        if s < l_r_cutoff_amount:
            left = start_index
            break

    # find the right margin of bottle
    right = segments_amount - 1
    # go from segment where most light pixel occur to left side
    for i in range(largest_counter, segments_amount - 1, divisor):
        # start index of 'merged' segments
        start_index = i

        # end index of 'merged' segments
        end_index = i + divisor
        # check the overflow
        if end_index > segments_amount - 1: end_index = segments_amount - 1

        # sum up the light pixels
        s = sum(counters[start_index: end_index])

        # check if the 'merged' segments are significant
        if s < l_r_cutoff_amount:
            right = end_index
            break

        # the margins are kinda known now

    # REMOVER
    # remover has divisor larger accuracy then the approximator from above

    # cutoff_amount = down boundary of white pixels to accept as significant segment
    cutoff_amount = 25

    # Go from left margin and then detect segment with amount of light pixels larger than cutoff_amount
    for i in range(left + 1, largest_counter + 1):
        if counters[i] > cutoff_amount:
            # left corner of bottle found
            left = i
            break
    for i in range(right - 1, largest_counter - 1, -1):
        if counters[i - 1] > cutoff_amount:
            # right corner of bottle found
            right = i
            break

    #
    ll, rr = detect_y_points(temp_image, segments_amount, divisor_value)

    # (left / right) is index ,step is the width of single segment
    return (int(left * step), ll[0]), (int(right * step), rr[0])


def detect_y_points(image, segments_amount, divisor_value):
    # similar to crop automatically x-axis
    height = image.shape[0]
    image = image.copy()
    image = cv.rotate(image, cv.ROTATE_90_COUNTERCLOCKWISE)
    # width = exact width of the image
    width = image.shape[1]

    # middle = exact middle of the image
    middle = int(width / 2)

    # segments = amount of segments to divide the image into
    segments = segments_amount

    # counters = how many white pixels occur in each segment
    counters = np.zeros((segments))

    # step = width of each segment
    # step times segment tells where segment begins (in pixels)
    step = width / segments

    # build the segments starting from middle
    # go in every iteration one step to right and one step to left
    for i in range(0, int(segments / 2)):
        # two segment at one time starting from middle
        right = crop(image, (middle + int(i * step), 0), (middle + int((i + 1) * step), height))
        left = crop(image, (middle - int((i + 1) * step), 0), (middle - int(i * step), height))

        # store the values at correct positions
        counters[int(segments / 2) - i - 1] = count_pixels_in_intervall(left, 100, 255)
        counters[int(segments / 2) + i] = count_pixels_in_intervall(right, 100, 255)
    # find where most white pixels occur
    largest_counter = np.where(counters == counters.max(0))[0][0]

    left = 0
    right = segments_amount

    cutoff_amount = 10
    # go from left segment to segment where most white pixels occur as long as more than cutoff amount pixels occur
    for i in range(left, largest_counter):
        if counters[i] > cutoff_amount:
            left = i
            break
    # go from right segment to segment where most white pixels occur as long as more than cutoff amount pixels occur
    for i in range(right, largest_counter, -1):
        if counters[i - 1] > cutoff_amount:
            right = i + 1
            break

    return (int(left * step), 0), (int(right * step), height)


def crop_automatically(image):
    l, r = detect_bottle_points(image, 4, 128, 32)
    cropped = crop(image, l, r)
    return cropped


def detect_bottle_points(image, resize_value, segments_amount, divisor_value):
    # automatically detects 2 corner points from rgb image
    # gets rgb image and returns 2
    bottle = cv.cvtColor(image.copy(), cv.COLOR_BGR2GRAY)
    # resize_factor ->  how (larger / smaller) should temporary processing image be
    # resize_factor bigger -> better performance but worse accuracy
    resize_factor = resize_value
    bottle_width = bottle.shape[1]
    bottle_height = bottle.shape[0]

    # concentrate on vertical lines
    kernel = np.ones((2, 10), np.uint8)

    # get the gradient
    bottle = cv.morphologyEx(bottle, cv.MORPH_GRADIENT, kernel)

    # resize image for better performance
    bottle = cv.resize(bottle, (int(bottle_width / resize_factor), (int(bottle_height / resize_factor))))

    l, r = detect_x_y_points(bottle, segments_amount, divisor_value)

    return (l[0] * resize_factor, l[1] * resize_factor), (r[0] * resize_factor, r[1] * resize_factor)


def detect_middle_rectangle_of_image(image, output_width):
    # is used to optimize performance of detecting fluid surface
    # returns middle rectangle of image

    temp_image = image.copy()
    height = temp_image.shape[0]
    width = temp_image.shape[1]

    middle = width // 2
    step = output_width // 2

    return crop(temp_image, (middle - step, 0), (middle + step, height))


def count_pixels_in_intervall(image, start, end):
    # gets grayscale or binary scale image and returns how many pixel in color range [start, end] occur
    # prepare helper array for better performance
    prep = np.zeros(256)
    for i in range(0, 256):
        if i >= start and i <= end:
            prep[i] = 1

    counter = 0
    for i in range(0, image.shape[0]):
        for j in range(0, image.shape[1]):
            # no need to use if prep[i] >= start and prep[i] <= end in every check iteration
            counter += prep[image[i][j]]
    return counter


def average_color(image):
    # returns average color from area between two points of image

    height = image.shape[0]
    width = image.shape[1]
    # NOTE: if area == 0 then dividing by zero can occur
    area = (height * width) + 1
    b, g, r = (0, 0, 0)

    for i in range(0, width):
        for j in range(0, height):
            b += image[j][i][0]
            g += image[j][i][1]
            r += image[j][i][2]

    b /= area
    g /= area
    r /= area

    return (int(b), int(g), int(r))


def estimate_image_color(image, areas_amount):
    #   find random areas from image and estimate the color
    #   helps to define what is a color of the fluid
    #   better not to use this function because it makes the program not determined

    b, g, r = (0, 0, 0)
    for i in range(0, areas_amount):
        # print(average_color(random_image_fragment(image)))
        tmp_average_color = average_color(random_image_fragment(image))
        b += tmp_average_color[0]
        g += tmp_average_color[1]
        r += tmp_average_color[2]
    b = int(b / areas_amount)
    g = int(g / areas_amount)
    r = int(r / areas_amount)

    return (b, g, r)


def random_point(image):
    # generate random point on surface of image
    return (random.randint(20, image.shape[0] - 20), random.randint(20, image.shape[1]) - 20)


def random_area(image):
    # generate two random points within the image
    left_top_corner = random_point(image)
    right_botton_corner_y = random.randint(left_top_corner[0] + 1, image.shape[0])
    right_botton_corner_x = random.randint(left_top_corner[1] + 1, image.shape[1])
    right_botton_corner = (right_botton_corner_y, right_botton_corner_x)
    print(f'{left_top_corner} --- {right_botton_corner}')
    return (left_top_corner, right_botton_corner)


def random_image_fragment(image):
    area = random_area(image)
    return image[area[0][0]: area[1][0], area[0][1]: area[1][1]]


def segment_image(image, segments_amount, mode):
    # returns array of segments points
    # mode == 0 segments will be built vertically from left to right
    # mode == 1 segments will be built horizontally from top to down
    height = image.shape[0]

    width = image.shape[1]

    segments_array = []

    if mode == 0:
        # vertical segmentation
        step = width / segments_amount
        for i in range(0, segments_amount):
            segments_array.append([(int(i * step), 0), (int((i + 1) * step), height)])
    else:
        # horizontal segmentation
        step = height / segments_amount
        for i in range(0, segments_amount):
            segments_array.append([(0, int(i * step)), (width, int((i + 1) * step))])

    return segments_array


def estimate_background_color(image):
    # returns average color of background
    temp_image = image.copy()

    height = temp_image.shape[0]
    width = temp_image.shape[1]
    temp_image = cv.resize(temp_image, (height // 8, width // 8))

    height = temp_image.shape[0]
    width = temp_image.shape[1]

    background = crop(temp_image, (0, 0), (width, height))
    background_color = average_color(background)
    preview(np.full((int(height), width, 3), fill_value=background_color, dtype=np.uint8))

    return background_color


def estimate_fluid_surface(image, segments_amount, background_color):
    # don't impact the original image
    tmp_image = image.copy()

    height = tmp_image.shape[0]
    width = tmp_image.shape[1]

    cv.imshow('image in estimate fluid surface', tmp_image)

    # if image has too small resolution than return 0.0 value
    if int(segments_amount) > int(height) or int(segments_amount) > int(width):
        return 0.5

    # resize for better performance
    resize_factor = int(width / segments_amount)
    tmp_image = cv.resize(tmp_image, (int(width / resize_factor), int(height / resize_factor)))
    # get points of segments
    segments_arr = segment_image(tmp_image, segments_amount, 1)

    # form sub images from corner points
    sub_images_arr = []
    for s in segments_arr:
        sub_images_arr.append(crop(tmp_image, s[0], s[1]))

    # build homogenous colors from sub images
    homogenous_color_images = []
    for im in sub_images_arr:
        b, g, r = average_color(im)
        homogenous_color_images.append(
            np.full((int(height / segments_amount), width, 3), fill_value=[b, g, r], dtype=np.uint8))

    # detect segments with similar colors and turn them into subsegments
    # subsegments with similar colors are stored in segments_arranged_by_color
    segments_arranged_by_color = []

    # variable in which current similar color segments are stored
    # this variable will be appended to segments_arranged_by_color after new segment color occurs
    temp_marge = [homogenous_color_images[0]]

    # cumulate average colors of segments and decide by it,
    # if next segment has similar color to average color of current segment
    # color accumulator stores rgb values of all similar color segments
    color_accumulator = []
    color_accumulator.append(int(homogenous_color_images[0][0][0][0]))
    color_accumulator.append(int(homogenous_color_images[0][0][0][1]))
    color_accumulator.append(int(homogenous_color_images[0][0][0][2]))
    # how many segments have similar color one after other
    # this variable helps to decide whether the next segment has similar color to actual segment or not
    color_accumulator_divisor = 1

    # collect average color of merged segments
    # the size of average_colors says how many important color segments occur in the image
    # the last element of average_colors is color of the fluid (if bottle not empty)
    # array contains info how many of total segments occur one after other with similar average color
    average_colors = []
    for i in range(0, segments_amount - 1):
        # homogenous_color_images[0] is first top segment

        # normalize  B,G,R to range [0:255]
        cb, cg, cr = color_accumulator[0], color_accumulator[1], color_accumulator[2]

        cb /= color_accumulator_divisor
        cg /= color_accumulator_divisor
        cr /= color_accumulator_divisor

        # work on integer values (the initial type is uint.8 which can lead to values overflows)
        cb, cg, cr = int(cb), int(cg), int(cr)

        # colors of next segment
        nb, ng, nr = homogenous_color_images[i + 1][0][0]

        # calculate differences between current average segment color and next segment color
        diff_b, diff_g, diff_r = int(cb) - int(nb), int(cg) - int(ng), int(cr) - int(nr)
        # if values are negative than normalize them
        diff_b, diff_g, diff_r = abs(diff_b), abs(diff_g), abs(diff_r)

        # 50/255 (around 25 percent) of color variation is legal for every color channel
        # i < segments_amount - 1 (- 1) because the last segment has to be added
        if diff_b < 50 and diff_g < 50 and diff_r < 50 and i < segments_amount - 1 - 1:
            # the next segment color is similar to current segment color
            temp_marge.append(homogenous_color_images[i + 1])
            color_accumulator[0] += int(homogenous_color_images[i + 1][0][0][0])
            color_accumulator[1] += int(homogenous_color_images[i + 1][0][0][1])
            color_accumulator[2] += int(homogenous_color_images[i + 1][0][0][2])

            color_accumulator_divisor += 1
        else:
            # found segment that has different color
            segments_arranged_by_color.append(temp_marge)

            # prepare helper for next color segment
            temp_marge = []
            temp_marge.append(homogenous_color_images[i + 1])

            # local temporary variable to build correct structure
            av_color = []
            # append average color to temporary variable
            av_color.append(cb)
            av_color.append(cg)
            av_color.append(cr)

            # local temporary variable to build correct structure
            av_color_data = []
            # append average color as array with 3 elements
            av_color_data.append(av_color)
            # store how many similar color segments occur one after another
            av_color_data.append(color_accumulator_divisor)

            # save to destination
            average_colors.append(av_color_data)

            # prepare accumulator for new segment color
            color_accumulator[0] = int(homogenous_color_images[i + 1][0][0][0])
            color_accumulator[1] = int(homogenous_color_images[i + 1][0][0][1])
            color_accumulator[2] = int(homogenous_color_images[i + 1][0][0][2])

            color_accumulator_divisor = 1

    # fluid color (if bottle is not empty)
    # (initially most down color)
    fluid_color = average_colors[len(average_colors) - 1][0]

    # go from down of bottle colors and search after color that could be the fluid color
    # accept color only if it occurs (segments_amount // 8) times one after another
    # it eliminates 1. potential reflection of background on the bottom of dish or bottle
    for i in range(len(average_colors) - 1, -1, -1):
        if average_colors[i][1] > segments_amount // 8:
            fluid_color = average_colors[i][0]
            break
    b, g, r = fluid_color

    # search for last top segment where fluid color occurs
    fluid_spotted = 0
    for i in range(0, segments_amount):
        # find color differences
        diff_b, diff_g, diff_r = abs(b - int(homogenous_color_images[i][0][0][0])), abs(
            g - int(homogenous_color_images[i][0][0][1])), abs(r - int(homogenous_color_images[i][0][0][2]))

        if (diff_b < 20 and diff_g < 20 and diff_r < 20):
            # found color that is similar to fluid color
            fluid_spotted = i
            break

    # it is still possible that bottle is empty (background color detected as fluid color)
    # check if the background color is similar to potential fluid color
    # if so then it means that the bottle is empty

    # background color is given as function parameter
    b, g, r = background_color

    # segments that definitely do not contain the background
    not_spotted_background_segments = []

    # go from bottom of the bottle to the top
    for i in range(0, segments_amount, ):
        # calculate color differences
        diff_b, diff_g, diff_r = abs(b - int(homogenous_color_images[i][0][0][0])), abs(
            g - int(homogenous_color_images[i][0][0][1])), abs(r - int(homogenous_color_images[i][0][0][2]))

        if diff_b > 30 or diff_g > 30 or diff_r > 30:
            # found color that is not similar to background color
            not_spotted_background_segments.append(sub_images_arr[i])

    # cv.imshow('not spotted background', cv.vconcat(not_spotted_background_segments))

    # most bottles have the neck that contains very little of the fluid
    # this instruction makes it easier to get 100 and 75 percent of fullness
    estimated_fullness = (segments_amount - fluid_spotted) / segments_amount
    if estimated_fullness > 0.69:
        estimated_fullness = 1.0
    elif estimated_fullness > 0.55:
        estimated_fullness = 0.75

    # normalize the fraction of bottle fullness
    bottle_fullness = fraction_to_nearest_value(estimated_fullness, [0.00, 0.25, 0.50, 0.75, 1.00])

    # check if bottle is full for real
    # if over 1/2 of bottle contains background then it means there is no fluid in the bottle

    # amount of spotted background color segments
    spotted_background_color_segments_amount = segments_amount - len(not_spotted_background_segments)
    # print('spotted_background_color_segments_amount', spotted_background_color_segments_amount)

    # check if bottle contains too much background
    if bottle_fullness >= 0.75 and spotted_background_color_segments_amount > segments_amount // 2:
        # print('too much background in the bottle')
        return 0.0

    # fluid spotted == 0 means that first top segment contains fluid
    # print('measured fullness:\t\t', (segments_amount - fluid_spotted) / segments_amount)
    return bottle_fullness


def fraction_to_nearest_value(fraction, values):
    # gets fraction and projects it to the nearest possible value from array values
    temp_values = values.copy()
    temp_values.sort()
    if fraction <= temp_values[0]:
        # fraction smaller than the smallest value
        return temp_values[0]

    # search after right interval
    for i in range(0, len(temp_values) - 1):
        if fraction > temp_values[i] and fraction <= temp_values[i + 1]:
            # interval found
            middle = (temp_values[i] + temp_values[i + 1]) / 2
            if fraction < middle:
                # fraction smaller than middle
                return temp_values[i]
            else:
                # fraction larger than middle
                return temp_values[i + 1]
    # fraction larger than largest value
    return temp_values[len(temp_values) - 1]


def preview(image):
    # preview the image
    cv.imshow(random_int(), image)


def random_int():
    return f'{random.getrandbits(16)}'


def time_benchmark(fun):
    # returns time needed to execute function f in seconds
    prev = int(tm.thread_time_ns())
    fun()
    return f'time needed to execute the function: {int(int(tm.thread_time_ns()) - prev) / (1000 * 1000 * 1000)}' + 'seconds'


#
hist_size = 256
# what range is allowed to show it on histogram?
hist_range = (0, 256)
# you should always choose the highest frequency manually!
highest_frequency = np.amax(10000)


# returns image of histogramm
def calculate_histogram(arr, i, color, thickness):
    # params for output histogram
    height = 400
    # take some nice 2^n value so rounding will work as expected!
    width = 512

    step_x = int(width / hist_size)
    step_y = (height / highest_frequency)

    hist = cv.calcHist([arr], [i], None, [hist_size], hist_range, accumulate=False)

    # init empty image
    destination = np.zeros((height, width, 3), dtype=np.uint8)
    # alpha = min, beta = max
    # why is it returning float numbers?
    for i in range(hist_size - 1):
        # the y vector was looking down -> normalization with - height
        cv.line(destination, (i * step_x, height - int(hist[i] * step_y)),
                ((i + 1) * step_x, height - int(hist[i + 1] * step_y)), color, thickness)
    #     you get image as result
    return destination


def calculate_histogram_BGR(arr, thickness):
    b = calculate_histogram(arr, 0, (255, 0, 0), thickness)
    g = calculate_histogram(arr, 1, (0, 255, 0), thickness)
    r = calculate_histogram(arr, 2, (0, 0, 255), thickness)
    return b + g + r


def shift_by_value(image, value):
    temp_image = image
    prep = np.zeros(256)
    for i in range(0, 256):
        prep[i] = i + value
        if prep[i] > 255:
            prep[i] = 255
        elif prep[i] < 0:
            prep[i] = 0
    for i in range(0, len(image)):
        for j in range(0, len(image[i])):
            temp_image[i][j] = prep[temp_image[i][j]]
    return temp_image


def quantize(image, limit):
    temp_img = image
    prep = np.zeros(256)
    step = int(255 / limit)
    counter = 0
    for i in range(0, limit):
        for j in range(0, step):
            prep[counter] = i * step + step / 2
            print(prep[counter])
            counter += 1

    for i in range(len(temp_img)):
        for j in range(len(temp_img[i])):
            temp_img[i][j] = prep[temp_img[i][j]]
    return temp_img


def correct_gamma(image, gamma_value):
    temp_image = image.copy()

    # init the values array
    # saves lots of resources
    prep = np.zeros(256)

    for i in range(0, len(prep)):
        prep[i] = pow(i / 255, gamma_value) * 255

    for i in range(0, len(temp_image)):
        for j in range(0, len(temp_image[i])):
            temp_image[i][j] = prep[temp_image[i][j]]
    return temp_image


def binarize(image, center):
    # below center white
    # above center black
    temp_img = image.copy()
    prep = np.zeros(256)
    for i in range(0, center):
        prep[i] = 0
    for i in range(center, 255):
        prep[i] = 255

    # no need to use if statements
    for i in range(0, len(temp_img)):
        for j in range(0, len(temp_img[i])):
            temp_img[i][j] = prep[temp_img[i][j]]
    return temp_img


def project_to_BGR(image, from_area_start, from_area_end, to_area_start, to_area_end):
    bb, gg, rr = cv.split(image);
    b_res = project_to(bb, from_area_start, from_area_end, to_area_start, to_area_end)
    g_res = project_to(gg, from_area_start, from_area_end, to_area_start, to_area_end)
    r_res = project_to(rr, from_area_start, from_area_end, to_area_start, to_area_end)
    return cv.merge([b_res, g_res, r_res])


def project_to(image, from_area_start, from_area_end, to_area_start, to_area_end):
    # shift all values to range [0 ; to_area_length]
    # project to the allowed range with multiplicator
    # shift values by to to_area_length

    if from_area_end < from_area_start or to_area_end < to_area_start:
        return
    temp_image = image.copy()

    from_area_length = from_area_end - from_area_start
    to_area_length = to_area_end - to_area_start
    projection_mult = to_area_length / from_area_length
    shift_by_value(temp_image, - (from_area_start))

    prep = np.zeros(256)
    for i in range(0, 256):
        prep[i] = i * projection_mult
        if prep[i] < 0:
            prep[i] = 0
        elif prep[i] > 255:
            prep[i] = 255

    for i in range(0, len(temp_image)):
        for j in range(0, len(temp_image[i])):
            temp_image[i][j] = prep[temp_image[i][j]]
    shift_by_value(temp_image, to_area_start)
    return temp_image


def equalize_histogram(image):
    # find area where to project
    start = 0
    end = 0
    cutoff_frequency_down = 0

    hist_gray = cv.calcHist([image], [0], None, [hist_size], hist_range)

    for i in range(0, hist_size - 1):
        if hist_gray[i] > cutoff_frequency_down:
            start = i
            break
    for i in range(hist_size - 1, 0, -1):
        if hist_gray[i] > cutoff_frequency_down:
            end = i
            break
    return project_to(image, start, end, 0, 255)


def equalize_histogram_bgr(image):
    # find area where to project
    start = 0
    end = 0
    cutoff_frequency_down = 0
    gray_channel = cv.cvtColor(image, cv.COLOR_BGR2GRAY)

    hist_gray = cv.calcHist([image], [0], None, [hist_size], hist_range)

    for i in range(0, hist_size - 1):
        if hist_gray[i] > cutoff_frequency_down:
            start = i
            break
    for i in range(hist_size - 1, 0, -1):
        if hist_gray[i] > cutoff_frequency_down:
            end = i
            break
    return project_to_BGR(image, start, end, 0, 255)
