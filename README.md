# Motivation
My goal was to write an algorithm that can detect a bottle in an image and read its fill level without using applications of artificial intelligence. 

My assumption was that the algorithm should recognize all types of transparent bottles, be robust against different liquid colors, be robust against different background colors, and be robust against the position of the bottle in the image. Additionally, my goal was to develop the algorithm in such a way that the labels on the bottles do not affect the final result.

I wanted to develop the algorithm with as few external libraries as possible. Moreover, I aimed to make my algorithm as responsive as possible, meaning that it can be extended to other datasets with minimal adjustments. My final goal was to achieve efficient speed with the algorithm so that it can provide accurate measurements in real-time.



## Visuals
![Alt Text](readme_sources/gif_bottles_terminal.gif)

## Installation
1. Install python3 on your pc ([Download](https://www.python.org/downloads/macos/)).
2. Download or clone this project.
3. Navigate to the directory with the main.py file.
4. Copy `pip install opencv-python`, paste it into your terminal and click enter.
5. Copy `pip install virtualenv`, paste it into your terminal and click enter.
6. Start the programm using `python3 main.py`.

## Requirements
1. You have to take a picture on homogenous background
2. The color of liquid has to be different than background color 
3. The bottle has to be transparent 
4. The bottle has to be paralel (+- 10%) photo right or left margin  
5. Usage of unix operating system


## Usage
To test the programm with your own pictures you have to follow theese steps: 
1. Use MacOS or any distribution of Linux (the programm has not been tested on windows)
2. take picture of your bottle (please read the requirements)
3. copy your image to `DetectorProject/example/Images` directory
4. add `Images/<name of your picture>;<correct fullness of the bottle>` to `DetectorProject/example/correct_fullness.txt`
5. go to the main function in `Detector_Project/DetectorProject/main.py` and follow the instructions
6. Start the programm using `python3 main.py`.



## How doe's it work?
- [technical explonation](readme_sources/technical_explonation.pdf)
- [visual explonation](readme_sources/_visual_explonation.pdf)

## External Licences
External licences used while developing this project: 
- https://opencv.org/license/
- https://docs.python.org/3/license.html


## License
The Open Use License
Version 1.0

Terms and Conditions for Copying, Distribution, and Modification
Permission Grant
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

Attribution
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. Proper attribution to the original author must be included in any distribution or derivative works of the Software.

No Warranty
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES, OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT, OR OTHERWISE, ARISING FROM, OUT OF, OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Personal Credit
The original author must be acknowledged in any use of the Software. This includes, but is not limited to, mentions in project documentation, about sections of applications, and other significant disclosures of derivative works.

## Contact

If you:

- have any inquiries,
- need further information, or
- would like to help me improve this repository,

I would be more than pleased if you contact me via my [Email](mailto:jakubdonut@gmail.com) or [Mobile](tel:+4915121732315)

